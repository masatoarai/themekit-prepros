
# Shopify Themekit Prepros

## Prerequisites

You will need [Shopify Themekit](https://shopify.github.io/themekit/) (Run following commands in your terminal.)

```
brew tap shopify/shopify
brew install themekit
```

Also you will need [Prepros](https://prepros.io/) to enable live reload development
(Download the web app following the link below)
[Download Link](https://prepros.io/downloads)

## Set indent size as 2 on your editor
In this project, we set indent size as 2. If you use VSCode you can simply add configure settings.
Create `.vscode/settings.json` on root directory and set like below.

```
  {
    "editor.tabSize": 2,
  }
```

## Add your theme as a project to Prepros
You can drag and drop the project folder or click add project button in the Prepros app

Shopify published their blog article how to set live reload dev environment with Prepros
[Live Reloading Shopify Themes](https://www.shopify.co.uk/partners/blog/live-reload-shopify-sass)

## Create configure file
You need API password, shop url and the theme’s ID you want to develop.
Once you got them, create `config.yml` file on root directory and add the settings for development and production environment below.

```
development:
  password: [your-api-password]
  theme_id: [your-theme-id]
  store: [your-shop.myshopify.com]
  ignore_files:
    - config/settings_data.json

production:
  password: [your-api-password]
  theme_id: [production-theme-id]
  store: [your-shop.myshopify.com]
  ignore_files:
    - config/settings_data.json
```

## Development

Run the following command and start development

```
$ npm run start
```

## Preview on browser
Once all set, open Prepros app and run Open Secure Preview (Shortcut Key following)
```
  ⌘ Command + K
```

It will open your browser with port `https://localhost:8848/`
As default the URL pointing to Production theme so you will need to type your theme id as query string.

```
  https://localhost:8848/?preview_theme_id=[YOUR THEME ID]
```

NOTE: This is one off action as once you points to your theme, Shopify stores it in Cookies so you don't need this for next a couple of days.

## Enable Allow invalid certificates for resources loaded from localhost.
If your browser block the site, you might need to change the Chrome's setting.
Visit [chrome://flags/](chrome://flags/)
`Enable` Allow invalid certificates for resources loaded from localhost.

## Deploy
You won't need to deploy to your theme as `theme watch` upload changes while you develop.

```
  $ npm run deploy (to your theme)
  $ npm run deploy_prod (to production)
```
