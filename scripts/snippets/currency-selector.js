import { getCookieValue } from '../helpers/cookies';

const selectors = {
  currencySelector: '[js-currency-selector="select"]',
};

let currentCurrency = 'GBP';

function setEventListners(currencySelector) {
  currencySelector.addEventListener('change', (event) => {
    const currenctPath = window.location.pathname;
    const selectedCurrency = event.target.value;
    const redirectPath = `${currenctPath}?currency=${selectedCurrency}`;
    return window.location.href = redirectPath;
  });
}

function setCurrencySelectors() {
  const currencySelectors = document.querySelectorAll(selectors.currencySelector);
  if (!currencySelectors.length) return;

  currentCurrency = getCookieValue('cart_currency');

  currencySelectors.forEach(selector => {
    const selectedOption = selector.querySelector(`[value="${currentCurrency}"]`);
    selectedOption.selected = true;

    setEventListners(selector);
  })
}

export function currencySelectorInit() {
  setCurrencySelectors();
}