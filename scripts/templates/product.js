const selectors = {
  app: '[js-product="app"]',
  json: '[js-product="json"]',
};

function init(app) {
  const productJson = app.querySelector(selectors.json);
  const product = Object.freeze(JSON.parse(productJson.innerHTML));
  console.log(product);
}

function onLoad() {
  const app = document.querySelector(selectors.app);
  if (!app) return;

  init(app);
}

onLoad();
