const selectors = {
  section: '[js-hero="section"]',
  data: '[js-hero="data"]',
};

function init(section) {
  // hero scripts here
}

function onLoad() {
  const section = document.querySelector(selectors.section);
  if (!section) return;

  init(section);
}

onLoad();
