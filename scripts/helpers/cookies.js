export function getCookieValue(name) {
  var value = document.cookie.match(`(^|;)\\s*${name}\\s*=\\s*([^;]+)`);
  return value ? value.pop() : '';
}